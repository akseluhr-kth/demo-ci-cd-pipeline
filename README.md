# **Demo CI/CD pipeline**
This repository is part of the demo presentation in *DD2482 DevOps course*, the following files and directories were used for the demo

- src: contains the application and tests used in the demo
- bitbucket-pipes.yml: deployment file used by bitbucket pipelines

## Create a CI/CD pipeline 

To replicate a similar CI/CD pipeline, do the following steps (*Note: you need an herouku account and a pipeline consisting of two apps setup at herouku*)

1. Fork this repository, you will find your cloned repo in **Repositories**
2. Go to pipelines and enable pipeline
	- You may need to enable two-step verification, if so go to personal settings of your account -> **two-step verification** and follow the guide there
3. Add the repository variables that are needed to deploy at herouku
	- Repository variables are added at Repository settings -> **repository variables**
	- Add: HEROKU_API_KEY: API key from your herouku account, HEROKU_STAGING: name of staging app, HEROKU_PROD: name of production app
4. Make a commit to trigger the pipeline
5. Go to pipelines to see the process

## Available Scripts
The repository contains a basic react application and lets you run the following react scripts.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!